## [1.4.1](https://gitlab.com/blissfulreboot/python/metadocuments/compare/v1.4.0...v1.4.1) (2020-10-29)


### Bug Fixes

* Force release and add Field to examples ([20c0632](https://gitlab.com/blissfulreboot/python/metadocuments/commit/20c06324803b83d0f1edf6ab29a4605d90f58b51))

# [1.4.0](https://gitlab.com/blissfulreboot/python/metadocuments/compare/v1.3.0...v1.4.0) (2020-10-28)


### Bug Fixes

* check field type with isinstance instead of strict type ([52dda7a](https://gitlab.com/blissfulreboot/python/metadocuments/commit/52dda7a09ed3eebe59859f000f5c9d1be1cbec0b))


### Features

* Add support for validators ([a06e94c](https://gitlab.com/blissfulreboot/python/metadocuments/commit/a06e94c2d69263218ca18af0dde78c75ddb82413))

# [1.3.0](https://gitlab.com/blissfulreboot/python/metadocuments/compare/v1.2.0...v1.3.0) (2020-10-27)


### Features

* add datastructure to allow certain special characters in the key ([704039c](https://gitlab.com/blissfulreboot/python/metadocuments/commit/704039c08efbacd0351c93e6d6c7e4e450a2b11f))

# [1.2.0](https://gitlab.com/blissfulreboot/python/metadocuments/compare/v1.1.1...v1.2.0) (2020-10-27)


### Features

* add support for + operand ([c1d735a](https://gitlab.com/blissfulreboot/python/metadocuments/commit/c1d735a13e28aa1085cd5c5c07e1b93a79018e4d))

## [1.1.1](https://gitlab.com/blissfulreboot/python/metadocuments/compare/v1.1.0...v1.1.1) (2020-10-27)


### Bug Fixes

* parent fields are not inherited ([e7d08c4](https://gitlab.com/blissfulreboot/python/metadocuments/commit/e7d08c48c527f7316bc16aa7fb65a23809989f75))
* typo ([ee85666](https://gitlab.com/blissfulreboot/python/metadocuments/commit/ee856666a64961839488c0d6a24efa1af6a9bbb6))
* typo fix ([065408a](https://gitlab.com/blissfulreboot/python/metadocuments/commit/065408a69c3eb5e2daea1594db44176401d6a6b4))

# [1.1.0](https://gitlab.com/blissfulreboot/python/metadocuments/compare/v1.0.3...v1.1.0) (2020-09-21)


### Features

* implement better JSON serializing support and FromKeywords helper class ([b3a30a0](https://gitlab.com/blissfulreboot/python/metadocuments/commit/b3a30a0c146b25f20d935fc01d03d001ce3a0c59))

## [1.0.3](https://gitlab.com/blissfulreboot/python/metadocuments/compare/v1.0.2...v1.0.3) (2020-09-19)


### Bug Fixes

* correct filename ([a91d9e9](https://gitlab.com/blissfulreboot/python/metadocuments/commit/a91d9e901ed8a7013af704b3e34078b9f975e488))

## [1.0.2](https://gitlab.com/blissfulreboot/python/metadocuments/compare/v1.0.1...v1.0.2) (2020-09-19)


### Bug Fixes

* Add MANIFEST.in to include certain non-code files ([4003743](https://gitlab.com/blissfulreboot/python/metadocuments/commit/4003743daec72b52d759a4e404a262ba63d75573))

## [1.0.1](https://gitlab.com/blissfulreboot/python/metadocuments/compare/v1.0.0...v1.0.1) (2020-09-19)


### Bug Fixes

* correct the classifier list in setup.py ([7dd3b2f](https://gitlab.com/blissfulreboot/python/metadocuments/commit/7dd3b2fad5c1b18abc3909cba36fab5aa6934cba))

# 1.0.0 (2020-09-19)


### Features

* First version of metadocument ([909594a](https://gitlab.com/blissfulreboot/python/metadocuments/commit/909594abf74568fd4c1515edf9d5ba8c7a58f1b9))
