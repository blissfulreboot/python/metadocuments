from metadocuments import Metadocument
import json

@Metadocument
class Simple:
    a = 1
    b = "a"
    c = [1, 2, 3]

@Metadocument
class SimpleWithMethod:
    a = 1
    b = "a"
    c = [1, 2, 3]

    def foobar(self):
        pass

@Metadocument
class SimpleWithPropertyMethod:
    a = 1
    b = "a"
    c = [1, 2, 3]

    @property
    def foobar(self):
        return "foobar"


@Metadocument
class SubDocument:
    a = 2
    b = "c"

@Metadocument
class TopLevelDocument:
    a = 1
    b = SubDocument()

def test_simple_class_to_dict():
    # Check that simple case passes, where class only contains some variables, can be converted to dict
    d = Simple().to_dict()
    expected_dict = { "a": 1, "b": "a", "c": [1, 2, 3]}
    assert json.dumps(d, sort_keys=True) == json.dumps(expected_dict, sort_keys=True)
    assert d.keys() == expected_dict.keys()

def test_simple_class_to_json():
    # Check that simple case passes, where class only contains some variables, can be converted to json
    d = Simple().to_json(indent=4)
    expected = \
'''{
    "a": 1,
    "b": "a",
    "c": [
        1,
        2,
        3
    ]
}'''
    assert d == expected

def test_simple_class_to_yaml():
    # Check that simple case passes, where class only contains some variables, can be converted to yaml
    d = Simple().to_yaml(indent=4)
    expected = \
'''a: 1
b: a
c:
- 1
- 2
- 3
'''
    assert d == expected

def test_simple_class_with_method_to_dict():
    # Check that the method will be ignored
    d = SimpleWithMethod().to_dict()
    expected_dict = { "a": 1, "b": "a", "c": [1, 2, 3]}
    assert json.dumps(d, sort_keys=True) == json.dumps(expected_dict, sort_keys=True)
    assert d.keys() == expected_dict.keys()

def test_simple_class_with_property_method_to_dict():
    # Check that property method's content will be added to the dict
    d = SimpleWithPropertyMethod().to_dict()
    expected_dict = {"a": 1, "b": "a", "c": [1, 2, 3], "foobar": "foobar"}
    assert json.dumps(d, sort_keys=True) == json.dumps(expected_dict, sort_keys=True)
    assert d.keys() == expected_dict.keys()

def test_document_with_subdocument():
    doc = TopLevelDocument()
    doc_dict = doc.to_dict()
    doc_json = doc.to_json(indent=4)
    doc_yaml = doc.to_yaml(indent=4)

    expected_dict = {"a": 1, "b": {"a": 2, "b": "c"}}
    expected_json = \
'''{
    "a": 1,
    "b": {
        "a": 2,
        "b": "c"
    }
}'''
    expected_yaml = \
'''a: 1
b:
    a: 2
    b: c
'''
    assert json.dumps(doc_dict, sort_keys=True) == json.dumps(expected_dict, sort_keys=True)
    assert doc_json == expected_json
    assert doc_yaml == expected_yaml
